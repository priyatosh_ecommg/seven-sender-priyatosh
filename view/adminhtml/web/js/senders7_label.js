/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */
require([
    'jquery',
    'mage/validation',
    'Magento_Ui/js/modal/confirm'
], function ($) {
    'use strict';
    $('#senders7_label').on('click', function () {
        var dataForm = $('#senders7_label').attr('data-href');
        $("body").append("<div id='seven_sender_label'></div><iframe></iframe>");
        if (dataForm) {
            $.ajax({
                url: dataForm,
                type: "GET",
                dataType: 'json',
                context: this,
                showLoader: true,
                complete: function (response) {
                    try {
                        var json = $.parseJSON(response.responseText) || {};
                        if (json.status == 'success') {
                            $("iframe").attr("src", json.url);
                        } else if (json.status == 'error') {
                            $('#seven_sender_label').empty();
                            $('#seven_sender_label').append(json.message);
                            $('#seven_sender_label').modal('openModal');
                        }
                    } catch (e) {
                        $('#seven_sender_label').empty();
                        $('#seven_sender_label').append(e.message);
                        $('#seven_sender_label').modal('openModal');
                    }
                },
                error: function () {
                    $('#seven_sender_label').empty();
                    $('#seven_sender_label').append('Something wrong...');
                    $('#seven_sender_label').modal('openModal');
                }
            });
        }
    });
});