<?php


namespace SevenSenders\Shipments\Model\Carrier;


use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;

class AbstractCarrier extends AbstractCarrierOnline
{
    /**
     * Rate result data
     *
     * @var Result
     */
    protected $_result;

    /**
     * Check if carrier has shipping tracking option available
     * @return bool
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    /**
     * Gets allowed methods
     * @return $this
     */
    public function getAllowedMethods()
    {
        return $this;
    }

    /**
     * Collects rates for shipping method
     * @param RateRequest $request
     * @return $this
     */
    public function collectRates(RateRequest $request)
    {
        return $this;
    }

    /**
     * Do shipment request to carrier web service, obtain Print Shipping Labels and process errors in response
     *
     * @param \Magento\Framework\DataObject $request
     * @return \Magento\Framework\DataObject
     */
    protected function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
        return $this;
    }

    /**
     * Get cgi tracking
     *
     * @param string[] $trackings
     * @return \Magento\Shipping\Model\Tracking\ResultFactory
     */
    protected function _getCgiTracking($trackings)
    {

        $result = $this->_trackFactory->create();
        foreach ($trackings as $tracking) {
            $status = $this->_trackStatusFactory->create();
            $status->setCarrier($this->_code);
            $status->setCarrierTitle($this->getConfigData('title'));
            $status->setTracking($tracking);
            $status->setPopup(1);
            $status->setUrl("#");
            $result->append($status);
        }

        $this->_result = $result;

        return $result;
    }

    /**
     * Get tracking
     *
     * @param string|string[] $trackings
     * @return Result
     */
    public function getTracking($trackings)
    {
        if (!is_array($trackings)) {
            $trackings = [$trackings];
        }

        $this->_getCgiTracking($trackings);

        return $this->_result;
    }
}