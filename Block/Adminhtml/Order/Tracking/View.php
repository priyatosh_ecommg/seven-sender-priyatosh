<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */
namespace SevenSenders\Shipments\Block\Adminhtml\Order\Tracking;

use Magento\Shipping\Block\Adminhtml\Order\Tracking\View as TrackView;
/**
 * Shipment tracking control form
 *
 */
class View extends TrackView
{
    /**
     * Prepares layout of block
     *
     * @return void
     */
    protected function _prepareLayout()
    {
        $onclick = "submitAndReloadAreaSenders($('shipment_tracking_info').parentNode, '"
            . $this->getSubmitUrl() . "')";
        $this->addChild(
            'save_button',
            'Magento\Backend\Block\Widget\Button',
            ['label' => __('Add'), 'class' => 'save', 'onclick' => $onclick]
        );
    }

}
