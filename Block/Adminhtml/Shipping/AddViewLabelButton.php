<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Block\Adminhtml\Shipping;

use SevenSenders\Shipments\Helper\Data as SevenSendersData;
use Magento\Backend\Block\Widget\Button\ButtonList as MagentoBackendButtonList;
use Magento\Backend\Block\Widget\Button\ItemFactory;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;

class AddViewLabelButton extends MagentoBackendButtonList
{
    /**
     * @var SevenSendersData
     */
    private $_helperData;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * ButtonList constructor.
     * @param ItemFactory $itemFactory
     * @param SevenSendersData $helperData
     * @param Registry $registry
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        ItemFactory $itemFactory,
        SevenSendersData $helperData,
        Registry $registry,
        UrlInterface $urlBuilder
    )
    {
        parent::__construct($itemFactory);
        $this->_helperData = $helperData;
        $this->registry = $registry;
        $this->urlBuilder = $urlBuilder;

        if ($shipment = $registry->registry('current_shipment'))
        {
            $button_url = $this->urlBuilder->getUrl(
                'sales/shipment/getshippinglabel',
                [
                    "shipment_id" => $shipment->getId()
                ]
            );
            $url = $this->_helperData->getSenders7LabelUrl($shipment);
            if ($this->_helperData->checkIfUrlExists($url)) {
                $this->add(
                    'senders7_label',
                    [
                        'label' => __('View Seven Senders Label'),
                        'class' => 'save',
                        'data_attribute' =>
                            [
                                'href' => $button_url
                            ]
                    ]
                );
            }
        }
    }
}
