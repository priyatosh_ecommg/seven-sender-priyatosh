<?php
/**
 * Copyright � 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Helper;

use Magento\CatalogInventory\Model\Stock;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DataObject;
use Magento\Framework\Locale\Resolver;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Shipment;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    /**
     * Prefix for system settings
     * @var string _configPrefix
     */
    protected $configPrefix = 'shipping/senders7/';

    /**
     * Client
     * @var SevenSenders
     */
    protected $client;

    /**
     * StoreManager
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * StoreInterface
     * @var StoreInterface
     */
    protected $store;

    /**
     * Stock
     * @var Stock
     */
    protected $stock;

    /**
     * Locale
     * @var Resolver
     */
    protected $locale;

    /**
     * Data constructor.
     * @param StoreManagerInterface $storeManager
     * @param StoreInterface $store
     * @param Stock $stock
     * @param Resolver $locale
     * @param Context $context
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        StoreInterface $store,
        Stock $stock,
        Resolver $locale,
        Context $context
    ) {
        $this->storeManager = $storeManager;
        $this->store = $store;
        $this->stock = $stock;
        $this->locale = $locale;
        parent::__construct($context);
    }

    /**
     * Fill fields by mapping
     *
     * @param string $field
     * @param DataObject $object
     * @param string $prefix
     *
     * @return bool|string
     */
    protected function _checkMapping($field, DataObject $object, $prefix = null)
    {
        switch ($prefix . $field) {
            // Special section (higher priority)
            case 'shipment_order_id':
                $result = $object->getOrder()->getRealOrderId();
                break;
            case 'order_order_id':

                // Order section
            case 'order_id':
                $result = $object->getRealOrderId();
                break;
            case 'order_url':
                $result = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK);
                break;
            case 'delivered_with_seven_senders':
                $result = $this->getClient()->getOptions('allowed_to_send_seven_senders_shipments')
                    && $object->getData('as_7senders');
                break;
            case 'order_date':
                $result = $object->getCreatedAt();
                break;
            case 'boarding_complete':
                $shipped = 0;
                foreach ($object->getAllVisibleItems() as $item) {
                    $shipped += $item->getQtyShipped();
                }
                $result = ($object->getData('total_qty_ordered') == $shipped);
                break;
            case 'language':
                $locale = explode('_', $this->locale->getLocale());
                $result = array_shift($locale);
                break;
            case 'promised_delivery_date':
                $result = $object->getCreatedAt();
                break;
            // Shipment section
            case 'tracking_code':
                $track = $object->getData('track_info');
                $result = $track ? $track->getData('track_number') : null;
                break;
            case 'package_no':
                $result = (int)$object->getOrder()->getShipmentsCollection()->count();
                break;
            case 'carrier':
                $track = $object->getData('track_info');
                if ($track) {
                    $country = $this->scopeConfig->getValue('carriers/' . $track->getData('carrier_code') . '/country');
                    $result = [
                        'name' => $track->getData('carrier_code'),
                        'country' => !$country ? $object->getOrder()->getShippingAddress()->getData('country_id') : $country,
                    ];
                } else {
                    $result = null;
                }
                break;
            case 'recipient_first_name':
                $result = $object->getOrder()->getShippingAddress()->getData('firstname');
                break;
            case 'recipient_last_name':
                $result = $object->getOrder()->getShippingAddress()->getData('lastname');
                break;
            case 'recipient_email':
                $result = $object->getOrder()->getData('customer_email');
                break;
            case 'recipient_address':
                $result = $object->getOrder()->getShippingAddress()->getData('street');
                break;
            case 'recipient_zip':
                $result = $object->getOrder()->getShippingAddress()->getData('postcode');
                break;
            case 'recipient_city':
                $result = $object->getOrder()->getShippingAddress()->getData('city');
                break;
            case 'recipient_country':
                $result = $object->getOrder()->getShippingAddress()->getData('country_id');
                break;
            case 'recipient_phone':
                $result = $object->getOrder()->getShippingAddress()->getData('telephone');
                break;
            case 'recipient_company_name':
                $result = $object->getOrder()->getShippingAddress()->getData('company');
                break;
            case 'weight':
                $result = (float)$object->getData('weight');
                break;
            case 'warehouse_address':
            case 'warehouse':
                $result = $this->stock->load(1)->getStockName();
                break;

            // Default section
            default:
                if ($prefix) {
                    $result = $this->_checkMapping($field, $object);
                } else {
                    $result = $object->getData($field);
                }
        }

        return $result;
    }

    /**
     * Retrieve configuration for module
     * @param string $path
     * @param string $scopeType
     * @return mixed
     */
    public function getConfig($path, $scopeType = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue($this->configPrefix . $path, $scopeType);
    }

    /**
     * Retrieve configuration
     * @param string $path
     * @param string $scopeType
     * @return mixed
     */
    public function getStoreConfig($path, $scopeType = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue($path, $scopeType);
    }

    /**
     * Is active flag
     * @return bool
     */
    public function isActive()
    {
        if ($this->getConfig('enabled') && $this->checkIfAvailableClient()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if SevenSender available
     * @return bool
     */
    public function checkIfAvailableClient()
    {
        try {
            if ($this->getConfig('access_key')) {
                if (!$this->client) {
                    $this->client = new SevenSenders($this->getConfig('access_key'));
                }

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get senders client instance
     * @return SevenSenders|bool
     */
    public function getClient()
    {
        try {
            if ($this->isActive()) {
                if (!$this->client) {
                    $this->client = new SevenSenders($this->getConfig('access_key'));
                }

                return $this->client;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;

        }
    }

    /**
     * Retrieve 7senders order by shipment
     * @param Shipment $shipment
     * @return array|bool
     */
    public function getOrder7ByShipment(Shipment $shipment)
    {
        $result = false;
        if ($shipment->getId()) {
            $sender = $this->getClient();
            $orderId = $shipment->getOrderId();
            $order = $sender->getOrder($orderId);
            if ($order) {
                $result = $order;
            }
        }

        return $result;
    }

    /**
     * Retrieve url for tracking
     * @param Shipment $shipment
     * @return string
     */
    public function getTrackingUrl(Shipment $shipment)
    {
        $url = '';
        $data = $shipment->getData('senders7');

        if ($data) {

            $info = $this->getSenders7Data($shipment, 'order7');
            if ($info) {
                $order = $info;
            } else {
                $order = $this->getOrder7ByShipment($shipment);
            }

            if ($order && isset($order['tracking_page_url'])) {
                $url = $order['tracking_page_url'];
            }
        }

        return $url;
    }

    /**
     * @param Shipment $shipment
     * @return string
     */
    public function getTrackingNo(Shipment $shipment)
    {
        $num = '';
        $info = $this->getSenders7Data($shipment, 'shipment7');

        if ($info && isset($info['reference_number'])) {
            $num = $info['reference_number'];
        }

        return $num;
    }

    /**
     * Retrieve if the button could be shown
     * @return bool
     */
    public function showButton()
    {
        $sender = $this->getClient();
        if ($sender) {
            return $sender->getOptions('allowed_to_send_seven_senders_shipments');
        } else {
            return false;
        }
    }

    /**
     * Checks if an url exists
     * @param string $url
     * @return bool
     */
    public function checkIfUrlExists($url)
    {
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_NOBODY, true);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if ($httpCode == 200) {
            return true;
        }

        return false;
    }

    /**
     * Retrieve a json data from the sender7 field
     * @param Shipment $shipment
     * @param string $fieldName
     * @return string
     */
    public function getSenders7Data(Shipment $shipment, $fieldName)
    {
        $result = '';

        if ($shipment->getId()) {
            $data = json_decode($shipment->getData('senders7'), true);
            if ($data[$fieldName]) {
                $result = $data[$fieldName];
            }
        }

        return $result;
    }

    /**
     * Retrieve a label url for a shipment
     * @param Shipment $shipment
     * @return mixed|string
     */
    public function getSenders7LabelUrl(Shipment $shipment)
    {
        $url = '';
        if ($this->isActive()) {
            $params = $this->getSenders7Data($shipment, 'shipment7');
            $sender = $this->getClient();

            if ($params && $sender) {
                $url = str_replace('/api/', '', $sender->getHost());
                $url .= is_array($params) && isset($params['label_url']) ? $params['label_url'] : '';
                $url .= '?bearer=' . $sender->getToken();
            }
        }

        return $url;
    }

    /**
     * Prepare data for sending to 7senders
     *
     * @param Order $order
     * @param array $fields
     *
     * @return array|bool
     */
    public function prepareOrder(Order $order, $fields)
    {
        $result = [];
        foreach ($fields as $field) {
            $result[$field] = $this->_checkMapping($field, $order, 'order_');
        }

        return $result;
    }

    /**
     * Prepare data for sending to 7senders
     *
     * @param Shipment $shipment
     * @param array $fields
     * @param array $order
     *
     * @return array
     */
    public function prepareShipment(Shipment $shipment, $fields, $order = [])
    {
        $result = [];
        foreach ($fields as $field) {
            $value = $this->_checkMapping($field, $shipment, 'shipment_');
            if ($value === null) {
                continue;
            }
            $result[$field] = $value;
        }

        if ((!isset($result['tracking_code']) && isset($result['carrier']))
            || $shipment->getData('as_7senders')
        ) {
            unset($result['tracking_code']);
            unset($result['carrier']);
        }
        if (isset($order['order_id'])) {
            $result['order_id'] = $order['order_id'];
        }

        return $result;
    }

    /**
     * Create an Order
     * @param Order $order
     * @return array|bool
     * @throws \Exception
     */
    public function createOrder(Order $order)
    {
        $sender = $this->getClient();

        $dataOrder = $this->prepareOrder($order, $sender->getFields('order', 'create'));
        $orderSeven = $sender->createOrder($dataOrder);

        return $orderSeven;
    }

    /**
     * Update a Seven Senders Shipment
     *
     * @param Shipment $shipment
     * @param null $shipmentSeven
     * @param null $orderSeven
     * @param bool $save
     * @return Shipment
     * @throws \Exception
     */
    public function updateShipment(Shipment $shipment, $shipmentSeven = null, $orderSeven = null, $save = true)
    {
        $info = [
            'order7' => $orderSeven,
            'shipment7' => $shipmentSeven,
        ];
        $json = json_encode($info);

        $shipment->setData('senders7', $json);
        if ($save) {
            $shipment->save();
        }

        return $shipment;
    }

    /**
     *
     * Create Seven Senders Shipment
     * @param Shipment $shipment
     * @param $orderSeven
     * @return array|bool|null
     * @throws \Exception
     */
    public function createShipment(Shipment $shipment, $orderSeven)
    {
        $sender = $this->getClient();

        $total_weight = 0;
        if ($shipment->getTotalWeight()) {
            $total_weight = $shipment->getTotalWeight();
        } else {
            foreach ($shipment->getItems() as $item) {
                if ($item->getWeight()) {
                    $total_weight += $item->getWeight();
                }
            }
        }

        $shipment->setData('weight', $total_weight);
        $dataShipment = $this->prepareShipment($shipment, $sender->getFields('shipment', 'create'), $orderSeven);
        $info = $this->getSenders7Data($shipment, 'shipment7');
        $sevenSendersShipment = $sender->createShipment($dataShipment);
        if (isset($sevenSendersShipment)) {
            $this->updateShipment($shipment, $sevenSendersShipment, $orderSeven);
        }

        return $sevenSendersShipment;
    }
}
